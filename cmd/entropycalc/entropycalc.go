// Copyright 2018 Travis Millican. All rights reserved.
// Use of this source code is governed by the MIT License which can be found
// in the LICENSE.md file.

// entropycalc is a command-line utility for computing the entropy of files.
//
// Basic usage: entropycalc FILE [ FILE [ ... ] ]
//
// entropycalc is a frontend wrapper around the packages in
// bitbucket.org/tmillican/go-entropy/pkg/entropy.
package main

import (
	"bitbucket.org/tmillican/go-entropy/pkg/entropy"
	"bitbucket.org/tmillican/go-entropy/pkg/entropy/shannon"
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/pprof"
)

// Emperically, a 1MB buffer seems to be the roughly the point of diminishing
// returns. Less than 256KB or more than 2MB seems to reduce performance
// slightly.
const bufSize int = 1048576

type cmdOptions struct {
	printNames bool
	printUnits bool
	useStdin   bool
	precision  int
	algo       string
	cpuProfile string
	memProfile string
	fileNames  []string
}

func main() {
	errLog := log.New(os.Stderr, "", 0)
	opts, ok := initFlags(errLog)
	var haveWarning bool
	if !ok {
		haveWarning = true
	}

	// Setup CPU profiling
	if opts.cpuProfile != "" {
		f, err := os.Create(opts.cpuProfile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	// "Normalize" the filename list to deal with useStdin
	var fileNames []string
	if opts.useStdin {
		fmt.Printf("Reading from stdin...")
		fileNames = make([]string, 1)
		// This value is ignored. We just need fileNames to be non-empty
		fileNames[0] = "--"
	} else {
		fileNames = opts.fileNames
	}

	// fileNames: Too many or too few
	if (opts.useStdin || (opts.memProfile != "")) && len(opts.fileNames) > 0 {
		errLog.Printf("[Warning] Ignoring extraneous arguments: %s ...",
			opts.fileNames[0])
		haveWarning = true
	}
	if len(opts.fileNames) == 0 {
		errLog.Fatal("[Fatal] No files specified. Use -h or --help for usage.")
	}

	var estimator entropy.Estimator
	switch opts.algo {
	case "shannon8":
		estimator = shannon.NewShannon8()
	default:
		errLog.Printf(
			"[Fatal] Unknown algorithm: '%s'. Valid algorithms are:\n",
			opts.algo)
		errLog.Println()
		errLog.Fatal(
			"\tshannon8\t\t- Shannon entropy with an 8-bit symbol size.\n")
	}
	estWriter := bufio.NewWriterSize(estimator, bufSize)
	// estWriter := bufio.NewWriter(estimator)

	for _, fileName := range fileNames {
		ok := digest(errLog, estWriter, fileName, opts.useStdin)
		if !ok {
			haveWarning = true
			continue
		}

		floatFmtString := fmt.Sprintf("%%.%df", opts.precision)
		fmt.Printf(floatFmtString, estimator.Entropy())
		if opts.printUnits {
			fmt.Print(" bits/byte")
		}
		if opts.printNames {
			fmt.Printf(" - %s", fileName)
		}
		fmt.Println()

		estimator.Reset()

		if opts.memProfile != "" {
			f, err := os.Create(opts.memProfile)
			if err != nil {
				log.Fatal(err)
			}
			pprof.WriteHeapProfile(f)
			f.Close()
			return
		}
	}

	// If we ran into any IO issues, give a non-zero exit code.
	if haveWarning {
		os.Exit(1)
	}
}

func initFlags(errLog *log.Logger) (options cmdOptions, ok bool) {
	var opts cmdOptions
	flag.BoolVar(&opts.printNames,
		"printnames", true,
		"include the file name in the output")

	flag.BoolVar(&opts.printUnits,
		"printunits", true,
		"include units of entropy in the output")

	flag.BoolVar(&opts.useStdin,
		"stdin", false,
		"read from stdin instead of files. If true, the file "+
			"list is ignored, if present.")

	flag.IntVar(&opts.precision,
		"precision", 6,
		"sets the number of decimal places to show in the output")

	flag.StringVar(&opts.algo,
		"algo", "shannon8",
		"The estimation algorithm to use. The default of "+
			"\"shannon8\" computes Shannon entropy with an 8-bit "+
			"symbol size.")

	flag.StringVar(&opts.cpuProfile, "cpuprofile", "", "write cpu profile to file")

	flag.StringVar(&opts.memProfile, "memprofile", "",
		"write memory profile to file. Exits after processing a single file.")

	flag.Parse()
	opts.fileNames = flag.Args()

	if opts.precision < 0 {
		errLog.Printf("[Warning] Invalid precision: %d. Must be > 0. "+
			"Using default value of 6.\n",
			opts.precision)
		opts.precision = 6
		return opts, false
	}

	return opts, true
}

func digest(errLog *log.Logger, estWriter *bufio.Writer, fileName string, useStdin bool) bool {
	// Open the file, or redirect to stdin per options
	var file *os.File
	if useStdin {
		file = os.Stdin
	} else {
		var err error
		file, err = os.Open(fileName)
		if err != nil {
			errLog.Printf(
				"[Warning] While opening '%s': %s\n",
				fileName, err)
			errLog.Printf("[Warning] Skipping '%s'.\n", fileName)
			return false
		}
	}

	_, err := estWriter.ReadFrom(file)
	if err != nil {
		errLog.Printf(
			"[Warning] While reading from '%s': %s\n",
			fileName, err)
		errLog.Printf("[Warning] Skipping '%s'.\n", fileName)
		return false
	}
	err = file.Close()
	if err != nil {
		errLog.Printf(
			"[Warning] While closing '%s': %s\n",
			fileName, err)
		return false
	}
	estWriter.Flush()
	return true
}
