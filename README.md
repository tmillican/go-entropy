# Golang Entropy

## Project Description

__Golang Entropy__ consists of two parts:

1. A collection of Go packages (in [pkg](/pkg) for estimating various kinds of
   entropy.

2. The `calcentropy` commmand-line utility (in [cmd](/cmd), which estimates the
   entropy of files.

Of course, you will need the [Go][1] build tools to compile the `calcentropy`
tool, but the tool itself is a fully self-contained, redistributable native
binary.

This is my first project in Go, so if you have suggestions for more "idiomatic"
code, don't hestitate to let me know.

[1]:https://golang.org

## Supported Entropy Estimates

Presently, __Golang Entropy__ only supports [Shannon entropy][2] with an 8-bit
symbol size.

[2]:https://en.wikipedia.org/wiki/Entropy_(information_theory)#Definition

## License

The __Golang Entropy__ project is released under the [MIT license](LICENSE.md).
