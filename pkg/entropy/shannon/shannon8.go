package shannon

import (
	"bitbucket.org/tmillican/go-entropy/pkg/entropy"
	"math"
)

func intMin(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func intMax(a, b int) int {
	if a > b {
		return a
	}
	return b
}

// digest8 provides the underlying state for a Shannon entropy estimator
// that operates on 8-bit symbols.
type digest8 struct {
	// digestCount is the number of message symbols that have been digested.
	digestCount int

	// freqs contains the individual symbol counts.
	freqs [256]uint

	// Cache of frequency counting "pads" and semaphore channels for Write
	// worker threads to use. This prevents reallocation on every invocation
	// of Write.
	wc writeCache
}

// New returns a new Estimator instance that computes Shannon entropy on the
// 8-bits-per-symbol level.
func NewShannon8() entropy.Estimator {
	d := digest8{}
	return &d
}

func (d *digest8) Reset() {
	d.digestCount = 0
	for i := range d.freqs {
		d.freqs[i] = 0
	}
}

type writeCache struct {
	// For convience and clarity. Equivalent to len(pads).
	size int
	pads [][]uint
	sems []chan interface{}
}

func (w *writeCache) expand(newSize int) {
	if newSize <= w.size {
		return
	}

	w.pads = make([][]uint, newSize)
	for i := range w.pads {
		w.pads[i] = make([]uint, 256)
	}

	w.sems = make([]chan interface{}, newSize)
	for i := range w.sems {
		w.sems[i] = make(chan interface{})
	}

	w.size = newSize
}

// After some more tweaking, it seems like any more than 2 threads causes a
// significant uptick in CPU time, without appreciably affecting the real
// time. That may be due to the fact that I have two physical cores on my
// MacBook Air, though. I'll need to do some more testing elsewhere to
// see.
//
// This will do in the meanwhile.
// Note to future self: https://blog.golang.org/profiling-go-programs
const minChunkSize int = 1024
const maxThreads int = 2

func (d *digest8) Write(p []byte) (n int, err error) {
	// For small inputs, skip all the convoluted thread setup and just
	// call the single-threaded implementation.
	if len(p) <= 2*minChunkSize {
		return d.writeSingleThreaded(p)
	}
	return d.writeMultiThreaded(p)
}

// Multi-threaded implementation of Write
func (d *digest8) writeMultiThreaded(p []byte) (n int, err error) {
	// Divide the input into chunks, respecting maximum thread count
	// and minimum chunk size.
	threadCount := intMin(maxThreads, len(p)/minChunkSize)
	if threadCount < 1 {
		threadCount = 1
	}
	chunkSize := len(p) / threadCount
	d.wc.expand(threadCount)

	// Launch worker threads to digest sub-slices of p. Each thread raises
	// a semaphore when done.
	for threadId := 0; threadId < threadCount; threadId++ {
		go func(id int) {
			start := id * chunkSize
			end := (id + 1) * chunkSize
			if id == threadCount-1 {
				end = len(p)
			}
			digest(p[start:end], d.wc.pads[id])
			d.wc.sems[id] <- nil
		}(threadId)
	}

	// Wait on the semaphores and update d.freqs with their results.
	for threadId := 0; threadId < threadCount; threadId++ {
		<-d.wc.sems[threadId]
		// TODO-maybe: there's room for more concurrency here, but it's
		// probably not worthwhile. We'll see what pprof thinks.
		for i, v := range d.wc.pads[threadId] {
			d.freqs[i] += v
			// Zero whatever was written to the cached pad so that
			// the previou line of code won't increment garbage on
			// the next multithreaded Write call
			d.wc.pads[threadId][i] = 0
		}
	}
	d.digestCount += len(p)
	return len(p), nil
}

// Single-threaded implementation of Write
func (d *digest8) writeSingleThreaded(p []byte) (n int, err error) {
	digest(p, d.freqs[:])
	d.digestCount += len(p)
	return len(p), nil
}

// Digests the bytes in p, updating the given frequency table
func digest(p []byte, pad []uint) {
	for _, sym := range p {
		pad[sym]++
	}
}

func (d *digest8) Entropy() float64 {
	var h float64
	// A sum over 0 terms is 0. Also, this prevents 0/0 below.
	if d.digestCount == 0 {
		return 0.0
	}
	for _, freq := range d.freqs {
		// Don't waste time doing floating-point arithmetic if the term
		// isn't going to contribute.
		if freq == 0 {
			continue
		}
		px := float64(freq) / float64(d.digestCount)
		h += px * math.Log2(px)
	}
	return -h
}
