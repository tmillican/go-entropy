// Copyright 2018 Travis Millican. All rights reserved.
// Use of this source code is governed by the MIT License which can be found
// in the LICENSE.md file.

package shannon

import (
	"io/ioutil"
	"math"
	"path/filepath"
	"testing"
)

// floatEquiv returns whether two floating point values are equal within some
// epsilon (arbitrarily 1e-10 because, why not?)
func floatEquiv(a, b float64) bool {
	if a == b {
		return true
	}
	return math.Abs(a-b)/math.Max(a, b) < 1e-10
}

func TestNewShannon8(t *testing.T) {
	est := NewShannon8()
	actual := est.Entropy()
	expected := 0.0
	if !floatEquiv(actual, expected) {
		t.Logf("Incorrect Entropy(), got %1.20f, want: %1.20f",
			actual, expected)
		t.Errorf("Because new Calculators should have zero entropy.")
	}
}

func TestReset(t *testing.T) {
	est := NewShannon8()
	est.Write([]byte("hello world"))
	est.Reset()

	expected := 0.0
	actual := est.Entropy()
	if !floatEquiv(actual, expected) {
		t.Logf("Incorrect Entropy(). Got %1.20f, wanted: %1.20f",
			actual, expected)
		t.Errorf("Because a freshly reset Calculator should have zero entropy.")
	}
}

func testEntropyWithWithPiecewiseWrites(t *testing.T,
	est *digest8, writeFn func([]byte) (int, error)) {

	writeFn([]byte("this is a test string"))
	expected := est.Entropy()

	est.Reset()
	writeFn([]byte("this "))
	writeFn([]byte("is "))
	writeFn([]byte("a test "))
	writeFn([]byte("string"))
	actual := est.Entropy()

	if !floatEquiv(actual, expected) {
		t.Logf("Incorrect Entropy(). Got %1.20f, wanted: %1.20f",
			actual, expected)
		t.Errorf("Because piecewise updates should be the same as a lump sum update.")
	}
}

func TestEntropyWithMultiThreadedPiecewiseWrites(t *testing.T) {
	est := NewShannon8().(*digest8)
	testEntropyWithWithPiecewiseWrites(t, est, est.writeMultiThreaded)
}

func TestEntropyWithSingleThreadedPiecewiseWrites(t *testing.T) {
	est := NewShannon8().(*digest8)
	testEntropyWithWithPiecewiseWrites(t, est, est.writeSingleThreaded)
}

func testEntropyDDT(t *testing.T, est *digest8, writeFn func([]byte) (int, error)) {
	for row := range entropyTestData(t) {
		est.Reset()
		writeFn(row.b)
		actual := est.Entropy()
		if floatEquiv(actual, row.expected) {
			continue
		}
		t.Errorf("Test data '%s': Incorrect Entropy(). "+
			"Got %1.20f, wanted %1.20f",
			row.desc, actual, row.expected)
	}
}

func TestEntropyWithMultiThreadedWritesDDT(t *testing.T) {
	est := NewShannon8().(*digest8)
	testEntropyDDT(t, est, est.writeMultiThreaded)
}

func TestEntropyWithSingleThreadedWritesDDT(t *testing.T) {
	est := NewShannon8().(*digest8)
	testEntropyDDT(t, est, est.writeSingleThreaded)
}

type entropyTestRow struct {
	b        []byte
	expected float64
	desc     string
}

// Validated against several other calculators. In particular:
//
// https://planetcalc.com/2476            <- high precision
// https://www.dcode.fr/shannon-index     <- medium precision
// http://www.shannonentropy.netmark.pl   <- low precision
func entropyTestData(t *testing.T) <-chan entropyTestRow {
	// First chapert of Moby Dick
	// Classy data, for a classy package.
	ishmaelIO := make(chan error)
	var ishmael []byte
	go func() {
		defer func() { close(ishmaelIO) }()
		path := filepath.Join("..", "..", "..", "testdata", "ishmael.txt")
		var err error
		ishmael, err = ioutil.ReadFile(path)
		if err != nil {
			ishmaelIO <- err
			close(ishmaelIO)
		}
		ishmaelIO <- nil
	}()

	// Friends, Romans, lend me your bits.
	juliusIO := make(chan error)
	var julius []byte
	go func() {
		defer func() { close(juliusIO) }()
		path := filepath.Join("..", "..", "..", "testdata", "julius.txt")
		var err error
		julius, err = ioutil.ReadFile(path)
		if err != nil {
			juliusIO <- err
		}
		juliusIO <- nil
	}()

	ch := make(chan entropyTestRow)
	go func() {
		defer func() { close(ch) }()

		ch <- entropyTestRow{nil, 0.0, "nil"}

		ch <- entropyTestRow{[]byte{}, 0.0, "empty buffer"}

		ch <- entropyTestRow{[]byte{0}, 0.0, "single-byte buffer"}

		ch <- entropyTestRow{[]byte("aaabbc"), 1.459147917027245, "aaabbc"}

		ch <- entropyTestRow{[]byte("Hello world!"), 3.0220552088742005, "hello world"}

		ch <- entropyTestRow{[]byte(
			"Lorem ipsum dolor sit amet, consectetur adipisicing elit, " +
				"sed do eiusmod tempor incididunt ut labore et dolore " +
				"magna aliqua."),
			4.022379320675357, "lorem ipsum"}

		ioErr := <-ishmaelIO
		if ioErr != nil {
			t.Fatalf("Could not load ishamel.txt: %s", ioErr)
		}
		ch <- entropyTestRow{[]byte(ishmael), 4.408127249965674, "ishmael.txt"}

		ioErr = <-juliusIO
		if ioErr != nil {
			t.Fatalf("Could not load julius.txt: %s", ioErr)
		}
	}()
	return ch
}
