// Copyright 2018 Travis Millican. All rights reserved.
// Use of this source code is governed by the MIT License which can be found
// in the LICENSE.md file.

/*
Package shannon provides Shannon entropy calculators.

See: https://en.wikipedia.org/wiki/Entropy_(information_theory)#Definition
*/
package shannon
