// Copyright 2018 Travis Millican. All rights reserved.
// Use of this source code is governed by the MIT License which can be found
// in the LICENSE.md file.

package entropy

import "io"

// Estimator is the common interface implemented by all entropy estimator.
//
// Entropy estimators are essentially specialized message digests that produce
// floating point numbers rather than integers. Accordingly, the Estimator
// interface is very similar to the hash.Hash interface.
type Estimator interface {
	// Write (via the embedded io.Writer interface) adds more data to the
	// running entropy calculation. It never returns an error.
	io.Writer

	// Reset resets the Estimator to its initial state.
	Reset()

	// Entropy returns the current entropy value. It does not change the
	// underlying entropy calculation state.
	Entropy() float64
}
