// Copyright 2018 Travis Millican. All rights reserved.
// Use of this source code is governed by the MIT License which can be found
// in the LICENSE.md file.

/*
Package entropy provides interfaces for entropy calculators
*/
package entropy
